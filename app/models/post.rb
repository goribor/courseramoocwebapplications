class Post < ActiveRecord::Base
  # has_many :comments - a way of creating FK (one-to-many relationship) between Post and Comment model classes
  # dependent: :destroy - will delete all posts comments as well, when the post gets deleted
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body
end
